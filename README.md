# LipsAi - Docu
`
import sys;
sys.path.append("...\lipsync\maya");
`

and load script

`
import blendshape_translation_qt;
reload(blendshape_translation_qt);
blendshape_translation_qt.run()
`

1. select your segmentation file and press the "predict" button, this workflow is valid for maya or web session
2. select meshes and press "Add Source"
3. select eyebrows adn blinking
4. press "Apply animation"

